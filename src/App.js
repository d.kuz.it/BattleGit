import './App.css';
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import Home from "./components/Home";
import Popular from "./components/Popular/Popular";
import Battle from "./components/battle/Battle";
import Navigation from "./components/Navigation";
import Results from "./components/battle/Results";

function App() {
  return <div className='container'>
    <BrowserRouter>
      <Navigation />
      <Routes>
        <Route path="/home" element= {<Home/>}/>
        <Route path="/popular" element= {<Popular/>}/>
        <Route path="/battle" element= {<Battle/>}/>
        <Route path="/battle/results" element= {<Results/>}/>

      </Routes>
    </BrowserRouter>




  </div>;
}

export default App;

import {Link, Outlet} from "react-router-dom";
import {NavLink} from "react-router-dom";
import '../App.css'
import Popular from "./Popular/Popular";


let oldNavArray = ["Home", "Popular", "Battle"];

let navArray = oldNavArray.map((el,index) => (
    <li key={index}>
    <NavLink to={el.toLowerCase()} className = { navData => navData.isActive ? 'active' : 'item' }>
        {el}
    </NavLink>
    </li>))
const Navigation = (props) => {
    return  <div >
        <ul className='nav'>
            {navArray}
            {/*<li>*/}
            {/*    <NavLink to='/home' //className = { navData => navData.isActive ? c.active : c.item }*/}
            {/*     > Home </NavLink>*/}
            {/*</li>*/}
            {/*<li>*/}
            {/*    <NavLink to='/popular' className = { navData => navData.isActive ? c.active : c.item }> Popular </NavLink>*/}
            {/*</li>*/}
            {/*<li>*/}
            {/*    <NavLink to='/battle' className = { navData => navData.isActive ? c.active : c.item }> Battle </NavLink>            </li>*/}

        </ul>
    </div>
}

export default Navigation;


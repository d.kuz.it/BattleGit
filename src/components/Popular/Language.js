export const Language = ({languages,setSelectedLanguage,preLoading,setPreLoading,selectedLanguage}) => {
    return <ul className="languages">
        {languages.map((language , index) =>(
            <li key={index}
                style={{ color: language === selectedLanguage ? '#d0021b' : '#000000' }}
                className={preLoading === true ? 'menu' : ''}
                onClick={()=> {setPreLoading(true); setSelectedLanguage(language)}}>
                {language}
            </li>
        ))}
    </ul>


}

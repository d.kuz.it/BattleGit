
import '../../App.css';
import {useState, useEffect} from "react";
import {fetchPopularRepos} from "../../api/Api";
import {Loader} from "../../Loader/Loader";
import {Persons} from "./Persons";
import { useSearchParams} from "react-router-dom";
import {Language} from "./Language";

let languages = ['All ','JavaScript ','Ruby ','Java ', 'Css ','Python ']

const Popular = (props) => {
    const [searchParams, setSearchParams] = useSearchParams()
    const singleValue = searchParams.get('language')
    const [selectedLanguage, setSelectedLanguage] = useState(singleValue ? singleValue : 'All ')
    const [loading, setLoading] = useState(false)
    const [preLoading, setPreLoading] = useState(false)
    const [repos, setRepos] = useState([])
    const [error, setError] = useState(null)


    useEffect(()=> {

        fetchPopularRepos(selectedLanguage).then((data) => {
                setSearchParams({language: selectedLanguage})
                setRepos(data)
                setLoading(true);
                setPreLoading(false);
        }
            )
            .finally(()=> setTimeout(() => {
                                setLoading(false);
                            }, 2000))
            .catch(error => setError(error))},[selectedLanguage])
    return <div >
        <Language  languages={languages} selectedLanguage={selectedLanguage} setPreLoading={setPreLoading}  setSelectedLanguage={setSelectedLanguage} preLoading={preLoading} />
        {loading? <Loader /> : <Persons repos={repos} />
    }

    </div>
}

export default Popular;



















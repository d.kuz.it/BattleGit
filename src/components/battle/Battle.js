
import '../../App.css';
import {PlayerInput} from "./PlayerInput";
import {useState} from "react";
import PlayerPreview from "./PlayerPreview";
import {Link} from "react-router-dom";


const Battle = () => {
    const [playerData, setPlayerData] = useState({
        playerOneName: "",
        playerTwoName: "",
        playerOneImage: null,
        playerTwoImage: null
    })
    const handleSubmit = (id, userName) => {
        setPlayerData((prevState) => ({
            ...prevState,
            [`${id}Name`]: userName,
            [`${id}Image`]: `https://github.com/${userName}.png?size200`,

        }))
    }
    const handleReset = (id) => {
        setPlayerData((prevState) => ({
            ...prevState,
            [`${id}Name`]: "",
            [`${id}Image`]: null,

        }))
    }

    return (
        <div><div className='row'>
            {playerData.playerOneImage ?
                <PlayerPreview
                    avatar={playerData.playerOneImage}
                    username={playerData.playerOneName}
                >
                    <button onClick={()=> {handleReset('id')}} className='reset'  >
                        reset
                    </button>
                </PlayerPreview> :
                <PlayerInput
                    id='playerOne'
                    label='player 1'
                    onSubmit={handleSubmit}
                />}

            {playerData.playerTwoImage ?
                <div className='column'>
                    <img src={playerData.playerTwoImage} alt={"avatar"} className='avatar' />
                    <h3>{playerData.playerTwoName}</h3>
                    <button className='reset' onClick={()=> {handleReset('playerTwo')}}>
                        reset
                    </button>
                </div> :
                <PlayerInput
                    id='playerTwo'
                    label='player 2'
                    onSubmit={handleSubmit}
                />}

        </div>
            {playerData.playerOneImage && playerData.playerTwoImage ?
                <Link to={{
                   pathname: 'results',
                    search: `?playerOneName=${playerData.playerOneName}&playerTwoName=${playerData.playerTwoName}`
                }} className='button'> battle  </Link>
                :
                null}
        </div>)


}

export default Battle;
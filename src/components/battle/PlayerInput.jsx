import {useState} from "react";
import  '../../App.css';
export const PlayerInput = ({id, label, onSubmit}) => {
    const [userName, setUserName] = useState(``)



    const handleSubmit = (event) => {
        event.preventDefault()
        onSubmit(id,userName)
    }


  return (<form className='column' onSubmit={handleSubmit} >
          <label className='header' htmlFor={label}>{label}</label>
                 <input
                  id={label}
                  type='text'
                  placeholder='GitHub UserName'
                  autoComplete='off'
                  value={userName}
                  onChange={(event) =>
                      setUserName(event.target.value)}/>
      <button className='button'
              type='submit'
              disabled={!userName.length}

      >
          Submit
      </button>
    </form>
  )
}


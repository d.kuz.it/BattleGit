import {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import {makeBattle} from "../../api/Api";
import {Player} from "./Player";
import {Loader} from "../../Loader/Loader";


const Results = () => {
    const location = useLocation()
    const [loading, setLoading] = useState(true)
    const [winner, setWinner] = useState(null)
    const [loser, setLoser] = useState(null)
    const [error, setError] = useState(null)



    useEffect(()=>{
         const param = new URLSearchParams(location.search)
         makeBattle([param.get('playerOneName'), param.get('playerTwoName')])
             .then(([winner, loser]) => {
                setWinner(winner);
                setLoser(loser);
             })
             .catch(error => setError(error))
             .finally(() => setLoading(false))
     },[])
    if(loading) {
        return <Loader />
    }

    if(error) {
        return <p>{error}</p>
    }
  return (
      <div className='row'>
          <Player
              label='Winner'
              score={winner.score}
              profile={winner.profile}
          />
          <Player
              label='Loser'
              score={loser.score}
              profile={loser.profile}
          />
      </div>
  )
}

export default Results;